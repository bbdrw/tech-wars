/**
 * File Name: itemFilter.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-05 : Created
 */

function showAllItemsFilterOne(max,min,order,numOfItems){

    //set options to an empty string
    var options = [];

    //if the numOfItems localstroage item is equalt to zero run the below code
    if(localStorage.getItem("numOfItems") == "1"){
        //set options to min and max
        options = [min,max];
    }
    //if the above statement is incorrect, set options to min,max and numOfItems
    else{
        options = [min,max,numOfItems];
    }

    //call the callback function after the myList.selectAllWithAllFilter function and pass in the options array and
    //callback function
    function callback(tx, results) {

        //set hmtlcode to an empty string
        var htmlcode = "";


        //foreach of the items returned from the filter set the name, image, description, price and quantity
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            htmlcode += "<li>\n" +
                "   <a data-role='button' data-theme='a' data-row-id=" + row['itemid'] + " href='#'>\n" +
                "       <img src=" + row['images'] + "\n width='100%' height='100%' alt='Thor'>\n" +
                "       <h3>" + row['name'] + "</h3>\n" +
                "       <p>" + row['description'] + "</p>\n" +
                "       <p> Price: " + row['price'] + "</p>\n" +
                "       <p>Quantity: " + row['quantity'] + "</p>\n" +
                "   </a>\n" +
                " </li>";
        }

        //set the list to the htmlcode and display it on the screen by refreshing the list element
        var ls = $("#lsAll");
        ls = ls.html(htmlcode);
        ls.listview("refresh"); //important

        //set an onclick listener for each item
        $("#lsAll a").on("click", clickHandler);

        function clickHandler() {
            //set the itemId to the clicked on item
            localStorage.setItem("itemId", $(this).attr("data-row-id") );
            //call the on_Page_Item_Desc_Click
            on_Page_Item_Desc_Click();
        }


    }
    //call the myList.selectAllWithAllFilter function and pass in the options array and callback function
    myList.selectAllWithAllFilter(options, callback);

}


function showAllItemsFilterTwo(max,order,numOfItems){
    //set options to an empty string
    var options = [];

    //if the numOfItems localstroage item is equal to 1 run the below code
    if(localStorage.getItem("numOfItems") == "1"){
        //set the options array to max
        options = [max];
    }
    //if the above statement is incorrect, set options to max and numOfItems
    else{
        options = [max,numOfItems];
    }

    //call the callback function after the myList.selectAllWithMaxFilter function and pass in the options array and
    //callback function
    function callback(tx, results) {

        //set hmtlcode to an empty string
        var htmlcode = "";


        //foreach of the items returned from the filter set the name, image, description, price and quantity
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            htmlcode += "<li>\n" +
                "   <a data-role='button' data-theme='a' data-row-id=" + row['itemid'] + " href='#'>\n" +
                "       <img src=" + row['images'] + "\n width='100%' height='100%' alt='Thor'>\n" +
                "       <h3>" + row['name'] + "</h3>\n" +
                "       <p>" + row['description'] + "</p>\n" +
                "       <p> Price: " + row['price'] + "</p>\n" +
                "       <p>Quantity: " + row['quantity'] + "</p>\n" +
                "   </a>\n" +
                " </li>";
        }

        //set the list to the htmlcode and display it on the screen by refreshing the list element
        var ls = $("#lsAll");
        ls = ls.html(htmlcode);
        ls.listview("refresh"); //important


        //set an onclick listener for each item
        $("#lsAll a").on("click", clickHandler);

        function clickHandler() {
            //set the itemId to the clicked on item
            localStorage.setItem("itemId", $(this).attr("data-row-id") );

            //call the on_Page_Item_Desc_Click
            on_Page_Item_Desc_Click();
        }


    }

    //call the myList.selectAllWithMaxFilter function and pass in the options array and callback function
    myList.selectAllWithMaxFilter(options, callback);
}


function showAllItemsFilterThree(min,order,numOfItems){

    //set options to an empty string
    var options = [];

    //if the numOfItems localstroage item is equalt to 1 run the below code
    if(localStorage.getItem("numOfItems") == "1"){
        //set the options to min
        options = [min];
    }
    //if the above statement is incorrect, set options to min and numOfItems
    else{
        options = [min,numOfItems];
    }

    //call the callback function after the myList.selectAllWithMinFilter function and pass in the options array and
    //callback function
    function callback(tx, results) {

        //set hmtlcode to an empty string
        var htmlcode = "";

        //foreach of the items returned from the filter set the name, image, description, price and quantity
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            htmlcode += "<li>\n" +
                "   <a data-role='button' data-theme='a' data-row-id=" + row['itemid'] + " href='#'>\n" +
                "       <img src=" + row['images'] + "\n width='100%' height='100%' alt='Thor'>\n" +
                "       <h3>" + row['name'] + "</h3>\n" +
                "       <p>" + row['description'] + "</p>\n" +
                "       <p> Price: " + row['price'] + "</p>\n" +
                "       <p>Quantity: " + row['quantity'] + "</p>\n" +
                "   </a>\n" +
                " </li>";
        }


        //set the list to the htmlcode and display it on the screen by refreshing the list element
        var ls = $("#lsAll");
        ls = ls.html(htmlcode);
        ls.listview("refresh"); //important



        //set an onclick listener for each item
        $("#lsAll a").on("click", clickHandler);

        function clickHandler() {
            //set the itemId to the clicked on item
            localStorage.setItem("itemId", $(this).attr("data-row-id") );
            //call the on_Page_Item_Desc_Click
            on_Page_Item_Desc_Click();
        }


    }
    //call the myList.selectAllWithMinFilter function and pass in the options array and callback function
    myList.selectAllWithMinFilter(options, callback);
}


function showAllItemsFilterFour(numOfItems){
    //set options to an empty string
    var options = [];

    //if the numOfItems localstroage item is equalt to 1 run the below code
    if(localStorage.getItem("numOfItems") == "1"){
        //set options to an empty string
        options = [];
    }
    //if the above statement is incorrect, set options to numOfItems
    else{
        options = [numOfItems];
    }


    //call the callback function after the myList.selectAllWithSortFilter function and pass in the options array and
    //callback function
    function callback(tx, results) {

        //set hmtlcode to an empty string
        var htmlcode = "";

        //foreach of the items returned from the filter set the name, image, description, price and quantity
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            htmlcode += "<li>\n" +
                "   <a data-role='button' data-theme='a' data-row-id=" + row['itemid'] + " href='#'>\n" +
                "       <img src=" + row['images'] + "\n width='100%' height='100%' alt='Thor'>\n" +
                "       <h3>" + row['name'] + "</h3>\n" +
                "       <p>" + row['description'] + "</p>\n" +
                "       <p> Price: " + row['price'] + "</p>\n" +
                "       <p>Quantity: " + row['quantity'] + "</p>\n" +
                "   </a>\n" +
                " </li>";
        }

        //set the list to the htmlcode and display it on the screen by refreshing the list element
        var ls = $("#lsAll");
        ls = ls.html(htmlcode);
        ls.listview("refresh"); //important

        //set an onclick listener for each item
        $("#lsAll a").on("click", clickHandler);

        function clickHandler() {
            //set the itemId to the clicked on item
            localStorage.setItem("itemId", $(this).attr("data-row-id") );
            //call the on_Page_Item_Desc_Click
            on_Page_Item_Desc_Click();
        }


    }
    //call the myList.selectAllWithSortFilter function and pass in the options array and callback function
    myList.selectAllWithSortFilter(options, callback);
}


function showAllItems() {
    //set options to an empty string
    var options = [];

    //call the callback function after the myList.selectAll function and pass in the options array and
    //callback function
    function callback(tx, results) {

        //set hmtlcode to an empty string
        var htmlcode = "";

        //foreach of the items returned from the filter set the name, image, description, price and quantity
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];


            htmlcode += "<li>\n" +
                "   <a data-role='button' data-theme='a' data-row-id=" + row['itemid'] + " href='#'>\n" +
                "       <img src=" + row['images'] + "\n width='100%' height='100%' alt='Thor'>\n" +
                "       <h3>" + row['name'] + "</h3>\n" +
                "       <p>" + row['description'] + "</p>\n" +
                "       <p> Price: " + row['price'] + "</p>\n" +
                "       <p>Quantity: " + row['quantity'] + "</p>\n" +
                "   </a>\n" +
                " </li>";
        }

        //set the list to the htmlcode and display it on the screen by refreshing the list element
        var ls = $("#lsAll");
        ls = ls.html(htmlcode);
        ls.listview("refresh"); //important


        //set an onclick listener for each item
        $("#lsAll a").on("click", clickHandler);

        function clickHandler() {

            //set the itemId to the clicked on item
            localStorage.setItem("itemId", $(this).attr("data-row-id") );

            //call the on_Page_Item_Desc_Click
            on_Page_Item_Desc_Click();
        }


    }

    //call the myList.selectAll function and pass in the options array and callback function
    myList.selectAll(options, callback);
}