/**
 * File Name: util.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-05 : Created
 */

function validateEmail(email){

    //if the email is valid return true
    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
        return true;
    }
    //if the email is not valid reutnr false
    else{
        return false;
    }

}

function validateCreateAccount(){
    // set errorMessage to an empty string
    var errorMessage = "";

    //set the options array to the email that was passed in through the form (to lowercase)
    var options = [$("#createEmail").val().toLowerCase()];

    //set the isEmailValid by calling the function validateEmail and pass in the email(lowercase)
    var isEmailValid = validateEmail($("#createEmail").val().toLowerCase());

    //call the callback function once the myLogin.select function is done
    function callback(tx, results) {
        var row = results.rows[0];

        //If there is no email that matches the one that was entered and it is a valid email and run the below code
        if(row == null && isEmailValid){

            //if the all of the information is valid run the below codee
            if(($("#createPassword").val() != null && $("#createPasswordRe").val() != null) && ($("#createPassword").val() != "" && $("#createPasswordRe").val() != "") && ($("#createPassword").val() == $("#createPasswordRe").val()) && ($("#createName").val() != null && $("#createName").val() != "" ) ){

                //set the createOptions array to the name of the user, password, and email the user inputted
                var createOptions = [$("#createName").val(),$("#createPassword").val(),$("#createEmail").val().toLowerCase()];

                //call the callback function once the myLogin.user function is done
                function callbackCreate(tx, results2){

                    //set the retrieveNewAccountOptions value tot he email created
                    var retrieveNewAccountOptions = [$("#createEmail").val().toLowerCase()];

                    //call the callBackRetreiveNewEmail once the myLogin.select function is done
                    function callBackRetreiveNewEmail(tx, results3){

                        //set the retrieveRow the results3.rows at position 0
                        var retrieveRow = results3.rows[0];

                        //set the usersid and email that was just created
                        localStorage.setItem("userLogin",retrieveRow['userid']);
                        localStorage.setItem("loginuseremail", retrieveRow['email']);

                        //if there are any errormessage, remove them
                        if(localStorage.getItem("errorMessageLogin") != null && localStorage.getItem("errorMessageLogin") != ""){
                            localStorage.removeItem("errorMessageLogin");
                        }

                        //call the on_Page_Home_Click function
                        on_Page_Home_Click();

                    }

                    //call the myLogin.select function and pass in the retrieveNewAccountOptions array and callBackRetreiveNewEmail
                    //callback function
                    myLogin.select(retrieveNewAccountOptions, callBackRetreiveNewEmail);

                }

                //call the myLogin.insert and pass in the createOptions array and callbackCreate function
                myLogin.insert(createOptions,callbackCreate);

            }
            //if any of the form is invalid run the below code
            else{

                //set isFalse to false
                var isFalse = false;

                //check each form input and whichever has invalid input add to errorMessage

                if($("#createPassword").val() == null || $("#createPassword").val() == "" ){
                    errorMessage = "The password is empty!<br/>";
                    isFalse = true;

                }
                else{
                    errorMessage = "The passwords do not match!<br/>";
                }

                if($("#createName").val() == null || $("#createName").val() == ""){

                    if(isFalse){
                        errorMessage += "The name cannot be empty!";
                    }
                    else{
                        errorMessage = "The name cannot be empty";
                    }

                }

                //call the displayErrorMessage function and pass in the errorMessage value and createAccountError string
                displayErrorMessage(errorMessage,"createAccountError");

                //return false
                return false;
            }

        }
        //if there is already an email with the same name or the email is invalid run the below code
        else{

            //set an error message for the email depending on if the email is valid, empty or already created

            if($("#createEmail").val() == null || $("#createEmail").val() == ""){
                errorMessage = "You have not entered in anything for the email!!";
            }
            else if(isEmailValid == false){
                errorMessage = "The email provided is not in a proper format!";
            }
            else{
                errorMessage = "The email provided is already taken!";
            }

            //call the displayErrorMessage function and pass in the errorMessage value and the createAccountError string
            displayErrorMessage(errorMessage,"createAccountError");

            //return false
            return false;

        }



    }

    //call the myLogin.select function with and pass in options array and callback function
    myLogin.select(options, callback);

}


function checkFavoriteValid(){

    //set the options array to the id of the logged in user and the id of the item
    var options = [localStorage.getItem("userLogin"), localStorage.getItem("itemId")];

    //call the callback function after the favorite.select method is done
    function callback(tx, results) {
        //set the row to results.rows at position 0
        var row = results.rows[0];

        //the item is not favorited
        if(row == null){

            //hide the unfavorite button and show the favorite button
            $("#btnFavorite").show();
            $("#btnUnFavorite").hide();

        }
        //the item is favorited
        else{

            //hide the favorite button and show the unfavorite button
            $("#btnFavorite").hide();
            $("#btnUnFavorite").show();

        }
    }

    //call the favorite.select function and pass in the options array and callback function
    favorite.select(options,callback);


}


function validateContactInfo(){

    //get the inputs from the contact form
    var name = $("#contactName").val();
    var email = $("#contactEmail").val();
    var desc = $("#contactDescription").val();

    //set isCorrect to true
    var isCorrect = true;
    //set message to an empty string
    var message = "";

    //check to see if any of the inputs from the contact form is invalid and if it is
    //add to the errorMessage
    if(name == null || name == ""){
        message += "The name is required <br>";
        isCorrect = false;
    }
    if(email == null || email == ""){
        message += "The email is required <br>";
        isCorrect = false;
    }
    if(desc == null || desc == ""){
        message += "The desc is required <br>";
        isCorrect = false;
    }

    //if there are errrors run the below code
    if(isCorrect == false){

        //set the html from the element that has an id of contactFormError to message
        document.getElementById('contactFormError').innerHTML = message;

    }
    //if there are no errors run the below code
    else{
        //alert the user that the email has been sent, change the page to pageHome awith a transition to pop
        alert("email sent!");
        $.mobile.changePage("#pageHome", {transition: 'pop'});
    }

}
