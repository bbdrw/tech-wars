/**
 * File Name: global.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-05 : Created
 */


function on_Page_Home_Click(){

    //Call the function page_show_home
    page_Home_show();

    //Change the page to the href that has a value of "#pageHome" and have a transition of pop
    $.mobile.changePage("#pageHome", {transition: 'pop'});

}

function page_Home_show(){

    //set the local storage item called "userPageOn" to "#pageHome"
    localStorage.setItem("userPageOn", "#pageHome");

    //If the localstorage item "userlogin" is not equal to null and not an empty string run the below code
    if(localStorage.getItem("userLogin") != null && localStorage.getItem("userLogin") != "" ){

        //hide the login button of the page and show the username and logout button
        document.getElementById('username').style.display = "block";
        document.getElementById('login').style.display = "none";

        document.getElementById('login').hidden = true;

        //set the html value of the element on the page with the id of email to the lcoalstorage item value of "loginuseremail"
        $("#email").html(localStorage.getItem("loginuseremail"));
    }
    //if the above statement is incorrect run the below code
    else{
        //hide the username and logout button and show the login button
        document.getElementById('username').style.display = "none";
        document.getElementById('login').style.display = "block";
    }

    //call the showAllItems to show items on the page
    showAllItems();
}

function on_Page_Cart_Click(){
    //call the cart attributes method
    cart_Attributes();

    //change the page to the href value that equals "#pageCart" and have a transition of slide
    $.mobile.changePage("#pageCart", {transition: 'slide'});

}

function cart_Attributes(){

    //set the localstorage item "userPageOn" to "#pageCart"
    localStorage.setItem("userPageOn", "#pageCart");

    //If the user is logged in, run the below code
    if(localStorage.getItem("userLogin") != null && localStorage.getItem("userLogin") != "" ){

        //show the users email and logout button
        //hide the logout button
        document.getElementById('usernameCart').style.display = "block";
        document.getElementById('loginCart').style.display = "none";


        //set the html of the element that has a id of "emailCart" to the value of the local storage item
        //with the name "loginuseremail"
        $("#emailCart").html(localStorage.getItem("loginuseremail"));

    }
    //if the user is not signed in hide the users infroamtion and show a login button
    else{
        document.getElementById('usernameCart').style.display = "none";
        document.getElementById('loginCart').style.display = "block";
    }

    //call the showCart method
    showCart();
}

function on_page_Favorite_Click(){
    //call the page_favorite_show function and then change the page to favorites
    page_favorite_show();
    $.mobile.changePage("#pageFavorite", {transition: 'slide'});
}

function page_favorite_show(){

    //set the localstorage item "userPageOn" to "#pageFavorite"
    localStorage.setItem("userPageOn", "#pageFavorite");

    //If the user is logged in run the below code
    if(localStorage.getItem("userLogin") != null && localStorage.getItem("userLogin") != "" ){

        //show the email and logout button and hide the login button
        document.getElementById('usernameFavorite').style.display = "block";
        document.getElementById('loginFavorite').style.display = "none";

        $("#favoriteErrorMessage").hide();

        $("#emailFavorite").html(localStorage.getItem("loginuseremail"));

        //call showFavorites function
        showFavorites();

    }
    //if the user is not logged in hide the username information and show login button
    else{
        document.getElementById('usernameFavorite').style.display = "none";
        document.getElementById('loginFavorite').style.display = "block";

        //clear the value of the element with the name "favoriteList"
        $("#favoriteList").empty();

        //show the error message stelling the user if they want to see their favorites they have
        //to sign in
        $("#favoriteErrorMessage").show();
    }

}

function on_Page_Contact_Click(){
    //call the show_contact_page function and then change the page to the href that has "#pageContact" with a transition
    //of slide
    show_contact_page();
    $.mobile.changePage("#pageContact", {transition: 'slide'});
}

function show_contact_page(){

    //set the localstorage item "userPageOn" to "#pageContact"
    localStorage.setItem("userPageOn", "#pageContact");

    //set the error message to an empty string fort he element with an id of "contactFormError
    document.getElementById('contactFormError').innerHTML = "";

    //reste the form that has an id of "contactform"
    $('#contactForm').trigger("reset");

    //set the src of the image element with an id of "imgTaken" to an empty string
    $("#imgTaken").prop("src", "");


    //if the user os logged in run the below code
    if(localStorage.getItem("userLogin") != null && localStorage.getItem("userLogin") != "" ){

        //hide the logout button and show the user email and logout button
        document.getElementById('usernameContact').style.display = "block";
        document.getElementById('loginContact').style.display = "none";

        $("#emailContact").html(localStorage.getItem("loginuseremail"));

        //set the email input box on the form to the users email
        $("#contactEmail").val(localStorage.getItem("loginuseremail"));

    }
    //if the user is not signed in run the below code
    else{
        //hide the users information and show the login button
        document.getElementById('usernameContact').style.display = "none";
        document.getElementById('loginContact').style.display = "block";

        //set the email input box ont he form to and empty string
        $("#contactEmail").val("");

    }
}

function on_Page_Profile_Click(){
    //call the page_profile_show function
    page_profile_show();
}

function page_profile_show(){

    //set the localstorage item "userpageon" to "#pageProfile"
    localStorage.setItem("userPageOn", "#pageProfile");

    //if the user is logged in run the below code
    if(localStorage.getItem("userLogin") != null && localStorage.getItem("userLogin") != "" ){

        //hide the login button and show the users email and logout button
        document.getElementById('usernameProfile').style.display = "block";
        document.getElementById('loginProfile').style.display = "none";

        $("#emailProfile").html(localStorage.getItem("loginuseremail"));

        //call the profileShow function
        profileShow();

        //change the page to profile with a transition of none
        $.mobile.navigate("#pageProfile", {transition: 'none'});
    }
    //if the user is not signed in run the below code
    else{
        //show login button and hide the usersemail and logout button
        document.getElementById('usernameProfile').style.display = "none";
        document.getElementById('loginProfile').style.display = "block";

        //call clearformLogin function
        clearformLogin();

        //change the page to login with a transition of none
        $.mobile.navigate("#pageLogin", {transition: 'none'});

    }
}

function on_Page_Item_Desc_Click(){
    //call page_Item_Desc_Show function
    page_Item_Desc_Show();

    //change page to item detail with a transition of slide
    $.mobile.changePage("#pageItemDetail", {transition: 'slide'});
}

function page_Item_Desc_Show(){

    //set the localstorage item "userPageOne" to "#pageItemDetail"
    localStorage.setItem("userPageOn", "#pageItemDetail");

    //If the user is logged in run the below code
    if(localStorage.getItem("userLogin") != null && localStorage.getItem("userLogin") != "" ){
        //show the users email and logout button and hide the login button
        document.getElementById('usernameDetail').style.display = "block";
        document.getElementById('loginDetail').style.display = "none";

        $("#emailDetail").html(localStorage.getItem("loginuseremail"));

        //call the checkFavoriteValid function
        checkFavoriteValid();
    }
    //If the user is not logged in run the below code
    else{

        //hide the users email and logout button and show login button
        document.getElementById('usernameDetail').style.display = "none";
        document.getElementById('loginDetail').style.display = "block";

        //hide the favorite button
        $("#btnFavorite").hide();
        $("#btnUnFavorite").hide();
    }


    //if the user is not logged in run the below code
    if(localStorage.getItem("userLogin") == null || localStorage.getItem("userLogin") == ""){
        //hide the add to cart button
        $("#btnAddCart").hide();
    }
    //if the user is logged in run the below code
    else{
        //show the add to cart button
        $("#btnAddCart").show();
    }

    //call displayItem function
    displayItem();
}

function checkLogin(){

    //If the user is not logged in run the below code
    if(localStorage.getItem("userLogin") == null || localStorage.getItem("userLogin") == ""){
        //call the clearformLogin function
        clearformLogin();

        //change the page to login and have a transition of none
        $.mobile.changePage("#pageLogin", {transition: 'none'});
    }

}

function pageCreateAccountVal(){
    //call the validateCreateAccount function
    validateCreateAccount();
}


function logoutUser(){
    //get rid of local storage items userLogin and loginuseremail
    localStorage.removeItem("userLogin");
    localStorage.removeItem("loginuseremail");

    //cal; tje setPageCallBack function
    setPageCallBack();
}



function addToCart(){
    //call the cartadd function
    cartAdd();
}


function show_about_page(){

    //set the localstorage item userPageOn to #pageAbout
    localStorage.setItem("userPageOn", "#pageAbout");

    //if the user is logged in run the below code
    if(localStorage.getItem("userLogin") != null && localStorage.getItem("userLogin") != "" ){

        //hide the login button and show the users email and logout button
        document.getElementById('usernamepageAbout').style.display = "block";
        document.getElementById('loginpageAbout').style.display = "none";

        $("#emailpageAbout").html(localStorage.getItem("loginuseremail"));

    }
    //if the user is not logged in run the below code
    else{
        //hide the logout button and usersemail and show the login button
        document.getElementById('usernamepageAbout').style.display = "none";
        document.getElementById('loginpageAbout').style.display = "block";

    }

    //change the page to about with transition fade
    $.mobile.changePage("#pageAbout", {transition: 'fade'});

}


function isCameraDeviceReady(){
    //set localStorage isDeviceReady to true
    localStorage.setItem("isDeviceReady", "true");

    //prevent the users phones back button from working with the app
    document.addEventListener("backbutton", function (p){
        p.preventDefault();
    }, false);

}

function takeEditablePhoto(){

    //if the device is ready to take a picture call capturePhoto()
    if(localStorage.getItem("isDeviceReady") != null && localStorage.getItem("isDeviceReady") != "" ){
        capturePhoto();
    }

}

function retrievePhoto(){
    //call retrievePhotoFromPhone function
    retrievePhotoFromPhone();
}

function init() {

    //call isCameraReady when the document is ready (phone is ready)
    $(document).on("deviceready", isCameraDeviceReady);


    //set all on click listeners for each of the login and logout buttons for each page
    $("#btnPhotoTake").on("click",takeEditablePhoto);
    $("#btnPhotoUpload").on("click",retrievePhoto);

    $("#btnAddCart").on("click",addToCart);

    $("#loginButton").on("click", checkLogin);
    $("#logoutButton").on("click",logoutUser);

    $("#loginButtonProfile").on("click", checkLogin);
    $("#logoutButtonProfile").on("click",logoutUser);

    $("#loginButtonDetail").on("click", checkLogin);
    $("#logoutButtonDetail").on("click",logoutUser);

    $("#loginButtonCart").on("click", checkLogin);
    $("#logoutButtonCart").on("click",logoutUser);

    $("#loginButtonContact").on("click", checkLogin);
    $("#logoutButtonContact").on("click",logoutUser);

    $("#loginButtonFavorite").on("click", checkLogin);
    $("#logoutButtonFavorite").on("click",logoutUser);

    $("#loginButtonpageAbout").on("click", checkLogin);
    $("#logoutButtonpageAbout").on("click",logoutUser);

    $("#loginButtonCreateAccount").on("click",checkLogin);

    $("#btnLogin").on("click", btnLoginSubmit);


    //set the rest of the on click listeners and the function to call when they are clicked
    $("#productFilter").on("click", filterItems);

    $("#rw_filter").on("click", checkFilter);

    $("#btnCreate").on("click", pageCreateAccountVal);


    $("#btnFavorite").on("click", addFavorite);
    $("#btnUnFavorite").on("click", deleteFavorite);

    $("#contactButton").on("click",validateContactInfo);


    //if the element with the id of rw_filter_section is not hidden call the filterItems function
    if(!document.getElementById('rw_filter_section').hidden){
        filterItems();
    }
    //if the above statement is false call the pge_Home_Show function
    else{
        page_Home_show();
    }

}


function clearformLogin(){
    //reset the form that has an id of loginForm
    $("#loginForm")[0].reset();
    //set html for the element with an id of loginError to an empty string
    document.getElementById('loginError').innerHTML = "";
}

function clearCreateNew(){
    //reset the form that has an id of createForm
    $("#createForm")[0].reset();

    //set html for the element with an id of createAccountError to an empty string
    document.getElementById('createAccountError').innerHTML = "";
}

function btnLoginSubmit(){
    //call the loginUser function
    loginUser();
}

function checkFilter(){

    //if the filter on the home page is clicked run the below code
    if(document.getElementById('rw_filter').checked){

        //show the filter section on the home page
        document.getElementById('rw_filter_section').hidden = false;

        //set the min and max values to empty string
        $("#rw_max").val("");
        $("#rw_min").val("");
    }
    //if the filter button is not checked run the below code
    else{
        //hide the filter form section
        document.getElementById('rw_filter_section').hidden = true;
        //call the page_Show_show function
        page_Home_show();
    }

    //if the filter section is hidden call the page_Home_show function
    if(document.getElementById('rw_filter_section').hidden == true){
            page_Home_show();
    }

}

function filterItems(){

    //get the values max,min,order, and numOfItems from the filter form
    var max = $("#rw_max").val();
    var min = $("#rw_min").val();
    var order = $("#rwselectorder").val();
    var numOfItems = $("#RWSelectNumOfItems").val();


    //if the order value is equal to one or 0 (show all items) run the below code
    if(order == 1 || order == 0 ){
        //set order localstorage item to nameA
        localStorage.setItem("order", "nameA");

    }
    //if the order value is equal to 2 run the below code
    else if (order == 2){
        //set order localstorage item to nameD
        localStorage.setItem("order", "nameD");
    }
    //if the order value is equal to 3 run the below code
    else if (order == 3){
        //set order localstorage item to priceA
        //set the order value to price ASC
        localStorage.setItem("order", "priceA");
        order = "price ASC";
    }
    //if the above statements are false run the below code
    else{
        //set order localstorage item to priceD
        //set the order value to price DESC
        localStorage.setItem("order", "priceD");
        order = "price DESC";
    }

    //if the value of numOfItems is equal to 1 run the below code
    if(numOfItems == 1){
        //set localstorage item numOfItems to 1
        localStorage.setItem("numOfItems", "1");
    }


    //if all of the inputs from the filter form are filled out, call the showALlItemmFilterOne and pass in
    //max,min,order, and numOfItems
    if((max != null && max != "") && (min != null && min != "")){
        showAllItemsFilterOne(max,min,order,numOfItems);
    }
    //else if max is filled in but min is not call the function showAllItemsFilterTwo and pass in
    //max,order,and numOfItems
    else if(max != null && max != ""){
        showAllItemsFilterTwo(max, order,numOfItems);
    }
    //else if min is filled in but max is not call the function showAllItemsFilterThree and pass in
    //min,order,and numOfItems
    else if(min != null && min != ""){
        showAllItemsFilterThree(min,order,numOfItems);
    }
    //else if the filter section is not enabled call the function showAllItemsFilterFour and pass in
    //numOfItems
    else{
        showAllItemsFilterFour(numOfItems);
    }



}

function initDB() {
    //set console.info to Creating Database
    console.info("Creating Database ...");

    //try to create a database with tables and records and catch any error that occur
    try {
        //clear the localstorage items
        localStorage.clear();

        //create the database
        DB.createDatabase();
        //if the database created is not null run the below code
        if (db) {
            //Clear all the items inserted into tables
            clearDatabaseItem();

            //create list,user,car,and favorites tables
            DB.createListTable();
            DB.createUserTable();
            DB.createCartTable();
            DB.createFavoriteTable();

            //fill th user,list,cart,and favorite tables
            DB.fillUserList();
            DB.fillListTable();
            DB.fillCartList();
            DB.fillFavoriteList();

            //set the localstorage item isConnected to true
            localStorage.setItem("isConnected", "true");
        }
        //if the database is not created set an error to cannot create tables database is not available
        else{
            console.error("Error: Cannot create tables : Database not available");
        }

       } catch (e) {
        //if there is any other error run the below code
        console.error("Error: (Fatal) Error in initDB(). Cannot proceed.");
    }
}


$(document).ready(function () {
    //when the document is ready call the initDB and init functions
    initDB();
    init();
});
