/**
 * File Name: ListItemsDAL.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-05 : Created
 */

var myCart={
    insert: function (options, callback) {
        function txFunction(tx) {
            var sql = "INSERT INTO cart(userid, itemid, quantity, price,name) VALUES(?,?,?,?,?);";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM cart WHERE userid=? AND itemid =?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    update: function (options, callback) {
        function txFunction(tx) {
            var sql = "UPDATE cart SET quantity=? WHERE userid=? AND itemid =?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    delete: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM cart WHERE userid=? AND itemid=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAll: function (options, callback) {
        function txFunction(tx) {
            //var sql = "SELECT * FROM items LIMIT ?;";
            var sql = "SELECT * FROM cart WHERE userid=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }
};


var myList ={
    insert: function (options, callback) {
        function txFunction(tx) {
            var sql = "INSERT INTO cart(userid, itemid, quantity) VALUES(?,?,?);";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    update: function (options, callback) {
        function txFunction(tx) {
            var sql = "UPDATE friend SET name = ?, fullName = ?, dob = ?, isFriend = ? WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: update transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    delete: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM friend WHERE id=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: delete transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM items WHERE itemid=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: select transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAll: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM items ORDER BY name ASC;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAllWithAllFilter: function (options, callback) {
        function txFunction(tx) {


            var sql = "";
            if(localStorage.getItem("numOfItems") == "1"){
                if(localStorage.getItem("order") == "nameA"){
                    sql = "SELECT * FROM items WHERE price BETWEEN ? AND ? ORDER BY name ASC;";


                }
                else if(localStorage.getItem("order") == "nameD"){
                    sql = "SELECT * FROM items WHERE price BETWEEN ? AND ? ORDER BY name DESC;";
                }
                else if(localStorage.getItem("order") == "priceA"){
                    sql = "SELECT * FROM items WHERE price BETWEEN ? AND ? ORDER BY price ASC;";
                }
                else{
                    sql = "SELECT * FROM items WHERE price BETWEEN ? AND ? ORDER BY price DESC;";
                }
            }
            else{
                if(localStorage.getItem("order") == "nameA"){

                    sql = "SELECT * FROM items WHERE price BETWEEN ? AND ? ORDER BY name ASC LIMIT ?;";
                }
                else if(localStorage.getItem("order") == "nameD"){
                    sql = "SELECT * FROM items WHERE price BETWEEN ? AND ? ORDER BY name DESC LIMIT ?;";

                }
                else if(localStorage.getItem("order") == "priceA"){
                    sql = "SELECT * FROM items WHERE price BETWEEN ? AND ? ORDER BY price ASC LIMIT ?;";
                }
                else{
                    sql = "SELECT * FROM items WHERE price BETWEEN ? AND ? ORDER BY price DESC LIMIT ?;";
                }
            }

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAllWithMaxFilter: function (options, callback) {
        function txFunction(tx) {
            var sql = "";
            if(localStorage.getItem("numOfItems") == "1"){
                if(localStorage.getItem("order") == "nameA"){
                    sql = "SELECT * FROM items WHERE price < ? ORDER BY name ASC;";

                }
                else if(localStorage.getItem("order") == "nameD"){
                    sql = "SELECT * FROM items WHERE price < ? ORDER BY name DESC;";
                }
                else if(localStorage.getItem("order") == "priceA"){
                    sql = "SELECT * FROM items WHERE price < ? ORDER BY price ASC;";
                }
                else{
                    sql = "SELECT * FROM items WHERE price < ? ORDER BY price DESC;";
                }
            }
            else{
                if(localStorage.getItem("order") == "nameA"){

                    sql = "SELECT * FROM items WHERE price < ? ORDER BY name ASC LIMIT ?;";
                }
                else if(localStorage.getItem("order") == "nameD"){
                    sql = "SELECT * FROM items WHERE price < ? ORDER BY name DESC LIMIT ?;";

                }
                else if(localStorage.getItem("order") == "priceA"){
                    sql = "SELECT * FROM items WHERE price < ? ORDER BY price ASC LIMIT ?;";
                }
                else{
                    sql = "SELECT * FROM items WHERE price < ? ORDER BY price DESC LIMIT ?;";
                }
            }

            localStorage.removeItem("order");
            localStorage.removeItem("numOfItems");

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAllWithMinFilter: function (options, callback) {
        function txFunction(tx) {

            var sql = "";
            if(localStorage.getItem("numOfItems") == "1"){
                if(localStorage.getItem("order") == "nameA"){
                    sql = "SELECT * FROM items WHERE price > ? ORDER BY name ASC;";


                }
                else if(localStorage.getItem("order") == "nameD"){
                    sql = "SELECT * FROM items WHERE price > ? ORDER BY name DESC;";
                }
                else if(localStorage.getItem("order") == "priceA"){
                    sql = "SELECT * FROM items WHERE price > ? ORDER BY price ASC;";
                }
                else{
                    sql = "SELECT * FROM items WHERE price > ? ORDER BY price DESC;";
                }
            }
            else{
                if(localStorage.getItem("order") == "nameA"){

                    sql = "SELECT * FROM items WHERE price > ? ORDER BY name ASC LIMIT ?;";
                }
                else if(localStorage.getItem("order") == "nameD"){
                    sql = "SELECT * FROM items WHERE price > ? ORDER BY name DESC LIMIT ?;";

                }
                else if(localStorage.getItem("order") == "priceA"){
                    sql = "SELECT * FROM items WHERE price > ? ORDER BY price ASC LIMIT ?;";
                }
                else{
                    sql = "SELECT * FROM items WHERE price > ? ORDER BY price DESC LIMIT ?;";
                }
            }

            localStorage.removeItem("order");
            localStorage.removeItem("numOfItems");

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAllWithSortFilter: function (options, callback) {
        function txFunction(tx) {
            var sql = "";
            if(localStorage.getItem("numOfItems") == "1"){
                if(localStorage.getItem("order") == "nameA"){
                    sql = "SELECT * FROM items ORDER BY name ASC;";

                }
                else if(localStorage.getItem("order") == "nameD"){
                    sql = "SELECT * FROM items ORDER BY name DESC;";
                }
                else if(localStorage.getItem("order") == "priceA"){
                    sql = "SELECT * FROM items ORDER BY price ASC;";
                }
                else{
                    sql = "SELECT * FROM items ORDER BY price DESC;";
                }
            }
            else{
                if(localStorage.getItem("order") == "nameA"){

                    sql = "SELECT * FROM items ORDER BY name ASC LIMIT ?;";
                }
                else if(localStorage.getItem("order") == "nameD"){
                    sql = "SELECT * FROM items ORDER BY name DESC LIMIT ?;";

                }
                else if(localStorage.getItem("order") == "priceA"){
                    sql = "SELECT * FROM items ORDER BY price ASC LIMIT ?;";
                }
                else{
                    sql = "SELECT * FROM items ORDER BY price DESC LIMIT ?;";
                }
            }

            localStorage.removeItem("order");
            localStorage.removeItem("numOfItems");
            //var sql = "SELECT * FROM items ORDER BY ? LIMIT ?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }

};

var myLogin={
    insert: function (options, callback) {
        function txFunction(tx) {
            var sql = "INSERT INTO user(name, password, email) VALUES(?,?,?);";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM user WHERE email=? LIMIT 1;";

            tx.executeSql(sql, options, callback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: select transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    checkPassword: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM user WHERE userid=? AND password=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: select transaction successful");
            //localStorage.setItem("myBool", "true");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    }

};

var user ={

    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM user WHERE userid=? LIMIT 1;";

            tx.executeSql(sql, options, callback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: select transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    }

};

var favorite ={
    insert: function (options, callback) {
        function txFunction(tx) {
            var sql = "INSERT INTO favorite(userid, itemid) VALUES(?,?);";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    delete: function (options, callback) {
        function txFunction(tx) {
            var sql = "DELETE FROM favorite WHERE userid=? AND itemid=?;";

            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: Insert transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    select: function (options, callback) {
        function txFunction(tx) {
            var sql = "SELECT * FROM favorite WHERE userid=? AND itemid=? LIMIT 1;";

            tx.executeSql(sql, options, callback, errorHandler);
        }

        function successTransaction() {
            console.info("Success: select transaction successful");
        }

        db.transaction(txFunction, errorHandler, successTransaction);
    },
    selectAll: function (options, callback) {
        function txFunction(tx) {
            //var sql = "SELECT * FROM favorite LIMIT ?;";
            var sql = "SELECT fa.itemid, fa.userid, it.name, it.images, it.description, it.quantity, it.price " +
                      "FROM favorite fa " +
                      "INNER JOIN items it ON fa.itemid = it.itemid " +
                      "WHERE fa.userid=? " +
                      "ORDER BY name ASC;";


            tx.executeSql(sql, options, callback, errorHandler);
        }
        function successTransaction() {
            console.info("Success: selectAll transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }


};