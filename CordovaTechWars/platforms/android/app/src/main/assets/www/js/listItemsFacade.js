/**
 * File Name: listItemsFacade.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-05 : Created
 */

function cartAdd(){

    //set the options to the id of the userlogged in and the id of the item the user wants to add
    var options = [localStorage.getItem("userLogin"), localStorage.getItem("itemId")];

    //call the callBack function when myCart.select is done
    function callback(tx, results) {
        //set the row to results.rows at the 0 position
        var row = results.rows[0];

        //if the item wanting to be added is not already in the cart for the user run the below code
        if(row == null || row == ""){

            //se the options to the id of the item
            var options2 = [localStorage.getItem("itemId")];

            //call the callBack2 function when myList.select is done
            function callback2(tx, results2){

                //set row2 to results2.rows at the 0 position
                var row2 = results2.rows[0];

                //set the price to the returned price from the table cart from the database
                var price = row2['price'];

                //set options3 to the userlogin id, id of the item wanting to be added, the quantity, price, and name of the item
                var options3 = [localStorage.getItem("userLogin"), localStorage.getItem("itemId"), 1, price,row2['name']];

                //call the callBack3 function when myCart.insert is done
                function callback3(tx, results2) {
                  //alert the user that the item has been added
                  alert("Item added to cart!");
                }
                //call the myCart.insert function and pass in the options3 array and callBack3 function
                myCart.insert(options3, callback3);

            }

            //call the myList.select function and pass in the options2 array and callBack2 function
            myList.select(options2, callback2);

        }
        //if the item being added is already in the cart run the below code
        else{

            //set the quantity to the returned quantity of the myCart.select method call and add one
            var quantity = row['quantity'] + 1;

            //set options2 to the quantity, id of the user logged in, and the id of the item
            var options2 = [quantity,localStorage.getItem("userLogin"), localStorage.getItem("itemId")];

            //call the callBack2 function when myCart.update is done
            function callback2(tx, results2) {
                //alert the user that there item has been added to cart
                alert("Item added to cart")

            }
            //call the myCart.update method and pass in options2 array and callback2 function
            myCart.update(options2, callback2);

        }

    }
    //call the myCart.select method and pass in options array and callback function
    myCart.select(options, callback);


}

function displayErrorMessage(errorcode,idName){
    //set htmlcode to the errorcode passed in
    var htmlcode = "<p style='color: lightcoral'>" + errorcode + "</p>";

    //set the html of the element of the page with an id of the passed in idName value
    document.getElementById(idName).innerHTML = htmlcode;

}

function loginUser(){
    // set errorMessage to an empty string
    var errorMessage = "";

    //set options array to the value of the email passed in from the login form and set it to lowercase
    var options = [$("#loginEmail").val().toLowerCase()];

    //call the callback function when the myLogin.select function is done
    function callback(tx, results) {
        //set row to results.rows at position 0
        var row = results.rows[0];

        //if the row is null then the email that was provided does not match any record for users
        if(row == null){
            //set errorMessage
            errorMessage = "The email provided is incorrect or you have not entered in anything!";
            //call the displayErrorMessage function and pass in the errorMessage and loginError string
            displayErrorMessage(errorMessage,"loginError");

            //return false;
            return false;
        }
        //if the email provided is found run the below code
        else{
            //set the localstorage items for email and password
            localStorage.setItem("loginuseremail", row['email'].toLowerCase());
            localStorage.setItem("loginuserpassword", row['password']);

            //if the password entered in by the user matches the password in the database run the below code
            if(localStorage.getItem("loginuserpassword") == $("#loginPassword").val()){

                //set the localstorage item userLogin to the userid of the logged in user
                localStorage.setItem("userLogin",row["userid"]);

                //remove the localstorage item loginuserpassword
                localStorage.removeItem("loginuserpassword");

                //if there has been an error message remove the localstorage item errorMessageLogin
                if(localStorage.getItem("errorMessageLogin") != null && localStorage.getItem("errorMessageLogin") != "" ){
                    localStorage.removeItem("errorMessageLogin");
                }

                //call the setPageCallBack fucntion
                setPageCallBack();
            }
            //if the password does not match the record returned from the database run the below code
            else{
                //set the errorMessage so the user known the password does not match our records
                errorMessage = "The password does not match our records";

                //call the displayErrorMessage function and pass in errorMessage and loginError string
                displayErrorMessage(errorMessage, "loginError");

                //return false
                return false;
            }

        }



    }
    //call the myLogin.select function and pass in the options array and callback function
    myLogin.select(options, callback);

}

function clearDatabaseItem() {

    //try to delete all of the tables in the database and if any errors occur alert the user
        try {
            DB.dropTables();
            DB.dropUsers();
            DB.dropCart();
            DB.dropFavorites();
        } catch (e) {
            alert(e);
        }

}


function showFavorites(){

    //set the options array to the id of the user who is logged in
    var options = [localStorage.getItem("userLogin")];

    //call the callback function when the favorite.selectAll function is done
    function callback(tx, results) {

        //set the htmlcode to an empty string
        var htmlcode = "";

        //for every item that is in the users favorites,  create a list item with the information of the
        //item. Set an onclick event listener for each list item created
        for (var i = 0; i < results.rows.length; i++) {
            var row = results.rows[i];

            htmlcode += "<li>\n" +
                "   <a data-role='button' data-theme='a' data-row-id=" + row['itemid'] + " href='#'>\n" +
                "       <img src=" + row['images'] + "\n width='100%' height='100%' alt='Thor'>\n" +
                "       <h3>" + row['name'] + "</h3>\n" +
                "       <p>" + row['description'] + "</p>\n" +
                "       <p> Price: " + row['price'] + "</p>\n" +
                "       <p>Quantity: " + row['quantity'] + "</p>\n" +
                "   </a>\n" +
                " </li>";
        }

        //set ls to the list element with an id of favoriteList
        var ls = $("#favoriteList");

        //set the hmlt to the list element tot he htmlcode
        ls = ls.html(htmlcode);

        //refresh the  list
        ls.listview("refresh");

        //when a item is clicked call the clickHandler function
        $("#favoriteList a").on("click", clickHandler);

        function clickHandler() {
            //set the localstorage itemId to the id of the item that was clicked
            localStorage.setItem("itemId", $(this).attr("data-row-id") );

            //call the on_Page_Item_Desc_Click function
            on_Page_Item_Desc_Click();
        }


    }
    favorite.selectAll(options,callback);
}


function deleteFavorite(){

    //set the options array to the users id, and the item id
    var options = [localStorage.getItem("userLogin"), localStorage.getItem("itemId")];

    //call the callback function when the favorite.delete function is done
    function callback(tx, results) {

        //alert the user that the item has been deleted from their favorites
        alert("item deleted from favorites");

        //if the user came form favorites page, change page to favorites with transition of fade
        if(localStorage.getItem("userPageOn") != null && localStorage.getItem("userPageOn") == "#pageFavorite" ){
            $.mobile.changePage("#pageFavorite", {transition: 'fade'});
        }
        //if the user has come from any other page call the on_Page_Item_Desc_Click function
        else{
            on_Page_Item_Desc_Click();
        }

    }
    //call the favorite.delete function and pass in the options array and callback function
    favorite.delete(options,callback);

}

function addFavorite() {
    //set the options array to the users id, and the item id
    var options = [localStorage.getItem("userLogin"), localStorage.getItem("itemId")];

    //call the callback function when the favorite.insertfunction is done
    function callback(tx, results) {

        //alert the user that the item has been added to their favorites
        alert("item added to favorites");

        //call the on_Page_Item_Desc_Click function
        on_Page_Item_Desc_Click();

    }
    //call the favorite.insert function and pass in the options array and callback function
    favorite.insert(options,callback);

}

function displayItem(){

    //set id to the id of the item that was saved
    var id = localStorage.getItem("itemId");

    //set the options array to id
    var options = [id];

    //call the callback function after the myList.select is done
    function callback(tx, results) {

        //set row to results.rows at position 0
        var row = results.rows[0];

        //set htmlcode to an empty string
        var htmlcode = "";

        //set the text of the element with an id of productName to the name of the item that is returned
        $("#productName").text(row['name']);

        //set the htmlcode to the values that are returned from the select statement
        htmlcode +=
            "<tr>\n" + "<td>" +
            "       <img width='200px' height='200px' alt='item' src=" + row['images'] + "\n" + "</td>" +

             "<td>" +  "<h4> Description:</h4>\n" +
            "       <p>" +  row['description'] + "</p>\n" +
            "           <h5> Price: $" + row['price'] + "</h5>\n" +
            "           <h5> Quantity: " + row['quantity'] + "</h5>\n" +"</td>" +"</tr>";

        //set the elements html on the page with an id of detail_Item to htmlcode
        document.getElementById('detail_Item').innerHTML = htmlcode;

    }
    //call the myList.select function and pass in options array and callback function
    myList.select(options, callback);


}


function showCart(){


    //if the user is not logged in run the below code
    if(localStorage.getItem("userLogin") == null ||  localStorage.getItem("userLogin") == ""){

        //show the login button and hide the users email and logout button
        document.getElementById('emptycart').style.display = "none";
        document.getElementById('emptycartLogin').style.display = "block";

        //empty the element with an id of user_cart
        $("#user_Cart").empty();
    }
    //if the user is logged in run the below code
    else{

        //set the options array to the logged in users id
        var options = [localStorage.getItem("userLogin")];

        //call the callback function when the myCart.selectAll is done
        function callback(tx, results) {
            //set row to results
            var row = results;

            //if there are no items in the cart run the below code
            if(row == null || row == ""){

                //show a message that you must be logged in to see favorites
                document.getElementById('emptycart').style.display = "block";
                document.getElementById('emptycartLogin').style.display = "none";

            }
            //if there are items in the cart run the below code
            else{

                //show the items in the cart and hide the message
                document.getElementById('emptycart').style.display = "none";
                document.getElementById('emptycartLogin').style.display = "none";

                //set htmlcode to an empty string
                var htmlcode = "";
                //set toal to 0
                var total = 0;

              //set the htmlcode to have tableheaders
                htmlcode +=
                    "<tr>\n" +
                    "       <th>\n" + "<font size='2'>Name </font>\n"  + "</th>" +
                    "       <th>\n" + "<font size='2'>Quantity </font>\n"  + "</th>" +
                    "       <th>\n" + "<font size='2'>Price</font>\n"  + "</th>" +
                    "<tr>\n";

                //get each of items name,quantity, and price of each item
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows[i];

                    htmlcode +=
                        "<tr style='width: 100%;' > <td>" +  "<p>" + row['name'] + "</p>\n" + "</td>" +
                        "<td> <input type=\"number\" min='0' style='width: 20%' class =\"cartQuant\" name=\"quantity\" data-row-quant=" + row['quantity'] + " value="+ row['quantity']+"><a data-role='button' data-theme='a'  data-row-id=" + row['itemid'] + " data-row-num=  " + i + " href='#'>UPDATE</a></td>" +
                        "<td>" +  "<p>$" + row['price'] + "</p>\n" + "</td>" + "</tr>";

                    var price = row['quantity'] * row['price'];

                    //increment the total price by total plus price
                    total = total + price;

                }

                //set the total price to the rounded number of total
                htmlcode +=  "<tr> <td colspan='2'>" +  "<p>Total Price</p>\n" + "</td>" +
                    "<td>" +  "<p>$" + Math.round(total * 100) / 100 + "</p>\n" + "</td>" + "</tr>";


                //set the element with an id of user_cart to htmlcode
                document.getElementById('user_Cart').innerHTML = htmlcode;

                //set an onclick listener for each update for each item, calling the clickHandler function
                $("#user_Cart a").on("click", clickHandler);

                function clickHandler() {

                    //set the cartItemid localstorage item to the id of the items id that is being updated
                    localStorage.setItem("cartItemId", $(this).attr("data-row-id") );

                    try {
                        //get the quantity of the selected item
                        var myQuant = parseInt($(this).siblings(".cartQuant").val());

                        //set finalQuant to myQuant
                        var finalQuant = myQuant;

                        //if the quantity is less than 0, alert the user that the quantity cannot be less than 0
                        if(finalQuant  < 0){
                            alert("Quantity cant be less than zero");
                        }

                        //if the quantity is equal to zero run the below code
                        else if (finalQuant  == 0){

                            //set options2 array to the id of the logged in user and the id of the cart item
                            var options2 = [localStorage.getItem("userLogin"),localStorage.getItem("cartItemId")];

                            //call the callback function once the myCart.delete function is done
                            function callback2(tx, results2) {

                                on_Page_Cart_Click();

                            }
                            //call the myCart.delete function and pass in options2 array and callback2
                            myCart.delete(options2, callback2);


                        }
                        //if the quantity is greater than zero than run the below code
                        else {
                            //set the options2 array to the id of the logged in user, and the id of the cart item
                            var options2 = [myQuant,localStorage.getItem("userLogin"),localStorage.getItem("cartItemId")];

                            //call the callback2 function once the myCart.update function is done
                            function callback2(tx, results2) {

                                //call the on_Page_Cart_Click fucntion
                                on_Page_Cart_Click();

                            }
                            myCart.update(options2, callback2);

                        }

                    }
                    catch(error){
                        alert("you must enter a number")
                    }

                }


            }

        }

        //call the myCart.selectAll function passing int he options array and callback function
        myCart.selectAll(options, callback);

    }

}

function profileShow(){

    //set id to the id of the logged in user
    var id = localStorage.getItem("userLogin");

    //set the option array to the id
    var options = [id];

    //call the callback function once the user.select function passing in the options array and callback function
    function callback(tx, results) {

        //set the row to results.row at position 0
        var row = results.rows[0];

        //set htmlcode to an empty string
        var htmlcode = "";

        //get the information from the returned record of the users profile
        htmlcode += "<tr>\n" +
            "       <th style='float: left' colspan='2'>\n" + "<font size='7'>Profile Information</font>\n"  + "</th>" +  "</tr>" +
            "<tr>\n<td>" +  "<h4> User Id:" +  row['userid'] + "</h4>\n" + "</td>" + "</tr>" +
            "<tr>\n<td>" +  "<h4> Name:" +  row['name'] + "</h4>\n" + "</td>" + "</tr>" +
            "<tr>\n<td>" +  "<h4> Email:" +  row['email'] + "</h4>\n" + "</td>" + "</tr>" +
            "<tr>\n<td>" +  "<h4> Password:"  +  row['password'] +  "</h4>\n" + "</td>" + "</tr>";

        //set the hmltcode to the element on the page with an id of profileTable
        document.getElementById('profileTable').innerHTML = htmlcode;

    }
    //call the user.select function and pass in the options array and callback function
    user.select(options, callback);
}


function setPageCallBack(){

    //set the page to the value of the localstorage item called userPageOn
    var page = localStorage.getItem("userPageOn");

    //check to see where the page has come from and call the appropriate function
    switch(page){

        case "#pageHome":
            on_Page_Home_Click();
            break;
        case "#pageProfile":
            on_Page_Profile_Click();
            break;
        case "#pageAbout":
            show_about_page();
            break;
        case "#pageLogin":
            clearformLogin();
            $.mobile.changePage("#pageLogin", {transition: 'none'});
            break;
        case "#pageItemDetail":
            on_Page_Item_Desc_Click();
            break;
        case "#pageCart":
            on_Page_Cart_Click();
            break;
        case "#pageFavorite":
            on_page_Favorite_Click();
            break;
        case "#pageContact":
            on_Page_Contact_Click();
            break;
        default:
            on_Page_Home_Click();

    }

}