function capturePhoto(){

    //set the camera options
    var cameraOptions= {
        quality: 70,
        allowEdit: true,
        destinationType: Camera.DestinationType.DATA_URL,

        sourceType: Camera.PictureSourceType.CAMERA
    };

    //once navigator.camera.getPicture method is done and is successful run the below code
    function onPictureSuccessful(imageData) {

        //set pictureImage to the elemetn on the page with an id of imgTaken
        var pictureImage = document.getElementById("imgTaken");
        //set the images src on the page to the newest taken image
        pictureImage.src = "data:image/jpeg;base64," + imageData;
    }

    //once navigator.camera.getPicture method is done and is unsuccessful run the below code
    function onPictureFialure(error) {
        //return an error alert message
        alert("Failed because of: " + error.message);
    }

    //call the navigator.camera.getPicture and pass in the onPictureSuccessful, onPictureFailure functions and cameraOptions array
    navigator.camera.getPicture(onPictureSuccessful, onPictureFialure, cameraOptions);

}


function retrievePhotoFromPhone(){

    //set the camera options

    var cameraOptions= {
        quality: 70,
        allowEdit: true,
        destinationType: Camera.DestinationType.FILE_URI,

        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
    };

    //once navigator.camera.getPicture method is done and is successful run the below code
    function onGetSuccessful(imageURI) {

        //set pictureImage to the element on the page with an id of imgTaken
        var pictureImage  = $("#imgTaken");
        //set the src ot the chosen image
        pictureImage.prop("src", imageURI);
    }

    //once navigator.camera.getPicture method is done and is unsuccessful run the below code
    function onGetFialure(error) {
        //return an error
        alert("Failed because of: " + error.message);
    }

    //call the navigator.camera.getPicture and pass in the onPictureSuccessful, onPictureFailure functions and cameraOptions array
    navigator.camera.getPicture(onGetSuccessful, onGetFialure, cameraOptions);

}