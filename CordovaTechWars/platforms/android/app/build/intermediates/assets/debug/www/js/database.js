/**
 * File Name: database.js
 *
 * Revision History:
 *       Ryan Wing, 2018-04-05 : Created
 */


var db;

/**
 * General purpose error handler
 * @param tx The transaction object
 * @param error The error object
 */
function errorHandler(tx, error) {
    console.error("SQL Error: " + tx + " (" + error.code + ") : " + error.message);
    return false;
}



var DB = {
    createDatabase: function(){
        var name= "TechWars DB";
        var version = "1.0";
        var displayName = "DB for TechWars app";
        var size =  2 * 1024 * 1024;

        function successCreate() {
            console.info("Success: Database created successfully");
        }

        db = openDatabase(name, version, displayName, size, successCreate);
    },
    dropTables: function () {

        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS items;";
            var options = [];
            function successDrop() {
                console.info("Success: table dropped successfully");
            }
            tx.executeSql(sql, options, successDrop, errorHandler);
        }
        function successTransaction() {
            console.info("Success: drop table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    dropUsers: function () {

        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS user;";
            var options = [];
            function successDrop() {
                console.info("Success: table dropped successfully");
            }
            tx.executeSql(sql, options, successDrop, errorHandler);
        }
        function successTransaction() {
            console.info("Success: drop table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    dropCart: function () {

        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS cart;";
            var options = [];
            function successDrop() {
                console.info("Success: table dropped successfully");
            }
            tx.executeSql(sql, options, successDrop, errorHandler);
        }
        function successTransaction() {
            console.info("Success: drop table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    dropFavorites: function () {

        function txFunction(tx) {
            var sql = "DROP TABLE IF EXISTS favorite;";
            var options = [];
            function successDrop() {
                console.info("Success: table dropped successfully");
            }
            tx.executeSql(sql, options, successDrop, errorHandler);
        }
        function successTransaction() {
            console.info("Success: drop table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createListTable: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS items( " +
                "itemid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "name VARCHAR(20) NOT NULL, " +
                "description VARCHAR(255), " +
                "price DOUBLE, " +
                "images LONGTEXT, " +
                "quantity INT);";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createUserTable: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS user( " +
                "userid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "name VARCHAR(20) NOT NULL, " +
                "password VARCHAR(255), " +
                "email VARCHAR(255));";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createCartTable: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS cart( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "userid VARCHAR(20) NOT NULL, " +
                "itemid VARCHAR(255) NOT NULL, " +
                "quantity int NOT NULL, " +
                "price DOUBLE NOT NULL, " +
                "name VARCHAR(255) NOT NULL, " +
                "FOREIGN KEY (userid) REFERENCES user(userid), " +
                "FOREIGN KEY (itemid) REFERENCES items(itemid));";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    createFavoriteTable: function(){
        function txFunction(tx) {
            var sql = "CREATE TABLE IF NOT EXISTS favorite( " +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "userid VARCHAR(20) NOT NULL, " +
                "itemid VARCHAR(255) NOT NULL, " +
                "FOREIGN KEY (userid) REFERENCES user(userid), " +
                "FOREIGN KEY (itemid) REFERENCES items(itemid));";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillFavoriteList: function(){
        function txFunction(tx) {
            var sql = "INSERT INTO favorite (userid, itemid) VALUES ('1','2');";
            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillCartList: function(){
        function txFunction(tx) {
            var sql = "INSERT INTO cart (userid, itemid,quantity, price,name) VALUES ('1','2', 1, 699,'HTC Vive');";
            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillUserList: function(){
        function txFunction(tx) {
            var sql = "INSERT INTO user (name, password, email) VALUES ('ryan', 'Teaching321', 'rhino_777@gmail.com');";
            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    },
    fillListTable: function(){
        function txFunction(tx) {
            var sql = "INSERT INTO items (name, description, price, images, quantity) VALUES ('Xbox One S', 'This is one of the newer xbox console which is almost half the size of the original xbox one', 259.99,'img/icon_Item.png', 5)," +
                "('HTC Vive', 'The htc vive is one of the hotest vr machines up to date', 699.00,'img/icon_Item.png', 3)," +
                "('NVIDIA SHIELD', 'This a older released console which allows you to play steam games right to your tv', 389.99, 'img/icon_Item.png', 2)," +
                "('Microsoft Xbox One Wireless Controller', 'Can be used for the Xbox One, Xbox One s, and the Xbox One X', 59.99, 'img/icon_Item.png', 5)," +
                "('Turtle Beach Recon Chat Headset - Xbox One', 'Great to be used in a group setup when taking on the battle with your closest friends, or listen to the riveting sounds of whatever game your playing', 29.99, 'img/icon_Item.png', 5)," +
                "('Playstaion 4 Pro 1TB Gaming Console', 'The newest generation of sonies gaming console is here allowing gamers to have more of a richer gaming experience', 499.99, 'img/icon_Item.png', 5)," +
                "('Sony Dual Shock Playstation 4 Controller', 'Use this to play on any playstation 4 console wirelessly', 74.99, 'img/icon_Item.png', 5)," +
                "('MSI GeForce GTX 1050 Ti Direct X', 'With VR capabilities this graphics card is definetely something to pick up', 309.99, 'img/icon_Item.png', 5)," +
                "('GIGABYTE AORUS Radeon Direct X Video Card', 'This is a nice graphics card to use when gaming on decent graphics', 479.99, 'img/icon_Item.png', 5)," +
                "('AMD RYZEN 1700 8-Core', 'This is a great processor from AMD', 59.99, 'img/icon_Item.png', 5)," +
                "('Intel Core i7-8700K Processor', 'The most up to date processor from intel', 464.99, 'img/icon_Item.png', 5)," +
                "('Cyberpower Desktop Computer', 'If you like gaming at max graphics settings then this machine is for you.', 2199.99, 'img/icon_Item.png', 5)," +
                "('Dell Desktop Inspiron 5675', 'This machine can be used for daily activities and some games', 1049.99, 'img/icon_Item.png', 5)," +
                "('ABS BattleBox Essentail Computer', 'The most up to date graphics card by NVIDIA and with the new intel i9 processor this machine can be used for whatever your needs are', 2049.99, 'img/icon_Item.png', 5)," +
                "('Nintendo Joy-Con Left and Right Neon', 'An attachment for the nintendo switch to take it on the go', 99.99, 'img/icon_Item.png', 5)," +
                "('Nintendo Switch Pro Controller', 'Used for the Nintendo Switch as a wireless controller', 79.99, 'img/icon_Item.png', 5)," +
                "('Nintendo Switch', 'Newer installment', 399.99,'img/icon_Item.png', 7);";

            var options = [];

            function successCreate() {
                console.info("Success: Table created successfully.");
            }

            tx.executeSql(sql, options, successCreate, errorHandler);
        }

        function successTransaction() {
            console.info("Success: Create table transaction successful");
        }
        db.transaction(txFunction, errorHandler, successTransaction);
    }


};





















